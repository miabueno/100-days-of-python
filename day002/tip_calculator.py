
print("Welcome to the tip calculator.")

gross = float(input("What was the total bill? $"))
tip_perc = float(input("What percentage tip would you like to give? 10, 12 or 15? "))
num_ppl = int(input("How many people to split the bill? "))

total = gross + (gross * (tip_perc / 100))
each_pay = round(total / num_ppl, 2)

print(f"Each person should pay: ${each_pay}")