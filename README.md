# 100 Days of Python

I am taking on the [100 Days of Code Challenge](https://www.udemy.com/course/100-days-of-code/) by Dr Angela Yu (The App Brewery)!

### Day `1` of `100`


|||
| :----: | :----: |
|**Start**| `Dec 5 2020` |
|**End**| `Mar 15 2021` |

***

##### My motivation behind this:

I'm a CS student and think I have a bit of imposter syndrome. Regardless of how many uni computing classes I've taken, I still feel incapable and that I'm somehow always behind everyone else - I hope	that by completing this course, outside of a tertiary-educaiton setting, I will have developed confidence in my own programming ability and have the project-based evidence to show for it. I also have a mad case of adhd and want to prove to myself that I can sit my ass down to focus and actually finish something I started.

##### Connect with me
* Watch me log my milestones, fk-ups & ask me questions on [twitter](#)
* See links below to completed projects